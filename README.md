## Smart hotel 

## Demo online 

Check it online 👉 [here](https://heuristic-ritchie-0f0fba.netlify.app/)


### Dev server
```yarn start```

### Build dev
```yarn build```

### Unit Tests

```yarn test```