import React from 'react'
import "./link.scss"

export interface IProps {
  title?: string;
}

const Link = ({title = ''}: IProps) => {
  return (
    <div className="link--container">
      <span>{title}</span>
    </div>
  )
}

export default Link
