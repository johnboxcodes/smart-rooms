import React from 'react'
import Link from './components/link';
import Logo from './components/logo';
import "./header.scss";

const Header = () => {
  return (
    <div className="header--container">
      <div className="brand">
        <Logo />
      </div>
      <div className="links">
        <Link title="Guest" />
      </div>
    </div>
  )
}

export default Header
