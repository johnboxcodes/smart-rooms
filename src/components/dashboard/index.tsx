import React, { useState } from 'react'
import { useEffect } from 'react'
import { guestRoomMatcher, Usage } from '../../helper'
import './dashboard.scss'

interface IProps {
  guests: number[];
}

const Dashboard = ({ guests }: IProps) => {
  const [availablePremiumRooms, setAvailablePremiumRooms] = useState(0)
  const [availableEconomyRooms, setAvailableEconomyRooms] = useState(0)
  const [occupancy, setOccupancy] =
    useState<Usage>()

  useEffect(() => {
    setOccupancy(guestRoomMatcher(availablePremiumRooms, availableEconomyRooms, guests))
    return () => {
    }
  }, [availablePremiumRooms, availableEconomyRooms, guests])

  const handleUserInput = (e: any) => {
    const name = e.target.name;
    const value = e.target.value;
    if (name === 'premium-rooms') {
      setAvailablePremiumRooms(value);
    }
    if (name === 'economy-rooms') {
      setAvailableEconomyRooms(value);
    }
  }

  return (
    <div className="dashboard--container">
      <h1 className="text-center mb-5">Occupancy Optimization</h1>
      <form>
        <div className="form-group">
          <div className="form-row">
            <h5>Premium rooms available</h5>
            <input className="col-1 col-sm-2"
              type='number'
              id="premium-rooms"
              name='premium-rooms'
              value={availablePremiumRooms}
              min={0}
              onChange={(e) => handleUserInput(e)} />
          </div>

          <div className="form-row mt-4">
            <h5>Economy rooms available</h5>
            <input className="col-1 col-sm-2"
              type='number'
              id="economy-rooms"
              name='economy-rooms'
              value={availableEconomyRooms}
              min={0}
              onChange={(e) => handleUserInput(e)} />
          </div>
        </div>
      </form>
      <hr className="my-3" />

      {(availablePremiumRooms > 0 || availableEconomyRooms > 0) && <div className="occupancy--wrapper">
        <div className="row">
          <h1 className="text-center mb-3">Revenue</h1>
        </div>
        <div className="row">
          <ul>
            <li>
              <span className="bold">Occupied Premium Rooms:</span> <span>{' '}{occupancy?.occupiedPremium}</span>
            </li>
            <li><span className="bold">Occupied Economy Rooms:</span><span>{' '}{occupancy?.occupiedEconomy}</span></li>
            <li><span className="bold">Total revenue:</span><span>{' '}{occupancy ? occupancy.totalPremium + occupancy.totalEconomy : 0}</span></li>
          </ul>
        </div>
      </div>}
    </div>
  )
}

export default Dashboard
