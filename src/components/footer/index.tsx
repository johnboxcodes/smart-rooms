import React from 'react'
import "./footer.scss"
const Footer = () => {
  return (
    <div className="footer--container">
      <span>© SMART HOST {new Date().getFullYear()}</span>
    </div>
  )
}

export default Footer
