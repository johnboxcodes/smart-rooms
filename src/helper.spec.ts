import { GUESTS } from './guests';
import { guestRoomMatcher } from './helper';

describe("Room matcher test Suite", () => {

  it('should match the available rooms with the guests for test', () => {
    //TEST 1
    expect(guestRoomMatcher(3, 3, GUESTS))
      .toEqual(
        {
          "occupiedPremium": 3,
          "occupiedEconomy": 3,
          "totalPremium": 738,
          "totalEconomy": 244,
        });
    //TEST 2
    expect(guestRoomMatcher(7, 5, GUESTS))
      .toEqual(
        {
          "occupiedPremium": 5,
          "occupiedEconomy": 5,
          "totalPremium": 954,
          "totalEconomy": 289,
        });
    //TEST 3
    expect(guestRoomMatcher(2, 7, GUESTS))
      .toEqual(
        {
          "occupiedPremium": 2,
          "occupiedEconomy": 5,
          "totalPremium": 583,
          "totalEconomy": 289,
        });

    //TEST 4
    expect(guestRoomMatcher(7, 1, GUESTS))
      .toEqual(
        {
          "occupiedPremium": 7,
          "occupiedEconomy": 1,
          "totalPremium": 1153,
          "totalEconomy": 45,
        });

  });
})

