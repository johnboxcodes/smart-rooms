export interface Usage {
  occupiedPremium: number;
  occupiedEconomy: number;
  totalPremium: number;
  totalEconomy: number;
}

export const guestRoomMatcher = (availablePremium: number, availableEconomy: number, guests: number[]): Usage => {
  const sortedByValue = guests.sort((a, b) => b - a);
  const economyGuests = sortedByValue.filter(guestPayment => guestPayment <= 100);
  const premiumGuests = sortedByValue.filter(guestPayment => guestPayment > 100);

  // if economy rooms are full and there's
  // empty premium rooms, upgrade the economy guest
  
  if (availableEconomy < economyGuests.length &&
    availablePremium > premiumGuests.length) {
      const matchedGuestsPremium = guests.slice(0, availablePremium);
      const matchedGuestsEconomy = guests.filter( ( el ) => !matchedGuestsPremium.includes( el ) ).slice(0, availableEconomy);
    return {
      occupiedPremium: matchedGuestsPremium.length,
      occupiedEconomy: matchedGuestsEconomy.length,
      totalPremium: matchedGuestsPremium.reduce((a, b) => a + b, 0),
      totalEconomy: matchedGuestsEconomy.reduce((a, b) => a + b, 0),
    }

  } else {
    const matchedGuestsPremium = premiumGuests.slice(0, availablePremium);
    const matchedGuestsEconomy = economyGuests.slice(0, availableEconomy);
    return {
      occupiedPremium: matchedGuestsPremium.length,
      occupiedEconomy: matchedGuestsEconomy.length,
      totalPremium: matchedGuestsPremium.reduce((a, b) => a + b, 0),
      totalEconomy: matchedGuestsEconomy.reduce((a, b) => a + b, 0),
    }
  }
}