import React from 'react';
import { Helmet, HelmetProvider } from 'react-helmet-async';

import './App.scss';
import Header from './components/header';
import Footer from './components/footer';
import Dashboard from './components/dashboard';
import { GUESTS } from './guests';
function App() {
  return (
    <HelmetProvider>
      <div className="app--container">
      <Helmet>
        <title>Room Occupancy Optimizer</title>
        <link rel="canonical" href="https://www.tacobell.com/" />
      </Helmet>
        <Header />
        <Dashboard guests={GUESTS} />
        <Footer />
      </div>
    </HelmetProvider>
  );
}

export default App;
